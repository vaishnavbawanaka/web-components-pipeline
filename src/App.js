import logo from './logo.svg';
import './App.css';
import HelloWorld from "./components/HelloWorld.Component";

function App() {
  return (
    <div className="App">
      <HelloWorld />
    </div>
  );
}

export default App;
